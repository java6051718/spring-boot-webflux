package com.curso.springboot.webflux.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curso.springboot.webflux.app.models.dao.ProductoDao;
import com.curso.springboot.webflux.app.models.documents.Producto;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/productos")
public class ProductoRestController {
	
	@Autowired
	private ProductoDao productoDao;
	
	@GetMapping()
	public Flux<Producto> findAll(){
		
		Flux<Producto> productos = productoDao.findAll().map(producto -> {
			producto.setNombre(producto.getNombre().toUpperCase());
			return producto;
		});
		
		return productos;
	}
	
	@GetMapping("/{id}")
	public Mono<Producto> findById(@PathVariable String id){

		/*
		Mono<Producto> producto = productoDao.findById(id);
		return producto;
		*/
		
		/*
		return productoDao.findById(id);
		*/
		
		Flux<Producto> productos = productoDao.findAll();
		
		Mono<Producto> producto = productos.filter(p -> p.getId().equals(id)).next();
		
		return producto;
	}
}
