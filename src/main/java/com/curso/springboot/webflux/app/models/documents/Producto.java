package com.curso.springboot.webflux.app.models.documents;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@Document(collection = "productos")
public class Producto {

	@Id
	private String id;
	
	@NonNull
	private String nombre;
	@NonNull
	private Double precio;
	private Date createAt;
}
