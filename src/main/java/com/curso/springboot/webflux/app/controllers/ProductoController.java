package com.curso.springboot.webflux.app.controllers;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.thymeleaf.spring6.context.webflux.ReactiveDataDriverContextVariable;

import com.curso.springboot.webflux.app.models.dao.ProductoDao;
import com.curso.springboot.webflux.app.models.documents.Producto;

import reactor.core.publisher.Flux;

@Controller
public class ProductoController {

	@Autowired
	private ProductoDao productoDao;
	
	@GetMapping({"/listar", "/"})
	public String litar(Model model) {
		
		Flux<Producto> productos = productoDao.findAll().map(producto -> {
			producto.setNombre(producto.getNombre().toUpperCase());
			return producto;
		});
		
		model.addAttribute("productos", productos);
		model.addAttribute("titulo", "Listado de productos");
		
		return "listar";
	}
	
	@GetMapping("/listar-datadirver")
	public String litarDataDriver(Model model) {
		
		Flux<Producto> productos = productoDao.findAll().map(producto -> {
			producto.setNombre(producto.getNombre().toUpperCase());
			return producto;
		}).delayElements(Duration.ofSeconds(1));
		
		model.addAttribute("productos", new ReactiveDataDriverContextVariable(productos,1));
		model.addAttribute("titulo", "Listado de productos");
		
		return "listar";
	}
	
	@GetMapping("/listar-full")
	public String litarFull(Model model) {
		
		Flux<Producto> productos = productoDao.findAll().map(producto -> {
			producto.setNombre(producto.getNombre().toUpperCase());
			return producto;
		}).repeat(5000);
		
		model.addAttribute("productos", new ReactiveDataDriverContextVariable(productos,1));
		model.addAttribute("titulo", "Listado de productos");
		
		return "listar";
	}
	
	@GetMapping("/listar-chunked")
	public String litarChunked(Model model) {
		
		Flux<Producto> productos = productoDao.findAll().map(producto -> {
			producto.setNombre(producto.getNombre().toUpperCase());
			return producto;
		}).repeat(5000);
		
		model.addAttribute("productos", new ReactiveDataDriverContextVariable(productos,1));
		model.addAttribute("titulo", "Listado de productos");
		
		return "listar-chunked";
	}
	
}
